#include<bits/stdc++.h>


#define pii pair<int,int>
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define eb emplace_back
#define pf push_front
#define pb2 pop_back
#define pf2 pop_front
#define line printf("\n")
#define pq priority_queue
#define rep(k,i,j) for(int k = (int)i;k<(int)j;k++)
#define repd(k,i,j) for(int k = (int)i;k>=(int)j;k--)
#define ll long long
#define ALL(a) a.begin(),a.end()
#define vi vector<int>

using namespace std;

double EPS = 1e-9;
int INF = 1e9+7;;
long long INFLL = 1e17;

//end of template

const int maxn = 1e5+5;
int n;
ll pw(ll a,ll b){
    if(b==0)return 1;
    if(b&1)return pw(a,b-1)*a;
    ll ret = pw(a,b>>1);
    return ret*ret;
}
ll Hash(ll a){
    return pw(2,a) + pw(3,a) + pw(5,a);
}

ll arr[maxn], prf[maxn], ori[maxn];

map<ll,vi> XOR;
map<ll,deque<int> > his;

int main(){
    cin>>n;
    
    long long ret = 0, cur = -INF;
    XOR[0].pb(-1);
    rep(k,0,n){
        cin>>arr[k]; ori[k] = arr[k];
        arr[k] = Hash(arr[k]);
        prf[k] = (k?prf[k-1]:0)^arr[k];
        XOR[prf[k]].pb(k);
        
        if(his[arr[k]].empty())his[arr[k]].pb(-INF);
    }
    
    rep(k,0,n){
        his[arr[k]].pb(k);
        if(his[arr[k]].size()>3)his[arr[k]].pf2();
        
        int left = his[arr[k]][0];
        cur = max(cur,left);
        int it = XOR[prf[k]][lower_bound(ALL(XOR[prf[k]]), cur)-XOR[prf[k]].begin()];
        
        ret = max(ret,k - it);
    }
    
    cout<<ret<<endl;
    
}
